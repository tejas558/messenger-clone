// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import {getAuth, GoogleAuthProvider} from 'firebase/auth';
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAjjgiH4WsHsoMIcSBMRdbsowWkfnln-xs",
  authDomain: "chatapp-fd5a8.firebaseapp.com",
  projectId: "chatapp-fd5a8",
  storageBucket: "chatapp-fd5a8.appspot.com",
  messagingSenderId: "796376667901",
  appId: "1:796376667901:web:2cbcdf1023e4b43bc35094",
  measurementId: "G-Q19ZBM1R4N"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
export const auth = getAuth(app);
export const provider = new GoogleAuthProvider();
export const db = getFirestore(app);