import React from "react";
import {auth, provider} from "../firebase-config.js"
import {signInWithPopup} from "firebase/auth";
import Cookies from "universal-cookie";
import "../styles/Auth.css";

const cookies = new Cookies();

export const Auth = (props) => {

    const {setIsAuth} = props;

    const signInWithGoogle = async () => {
        try {
            const result = await signInWithPopup(auth, provider);
            console.log(result);
            cookies.set("auth-token", result.user.refreshToken);
            setIsAuth(true);
        }
        catch(error) {
            console.log(error);
        }
    }
    return (
    <div className="auth">
        <p>Sign in with Google to continue</p>
        <button onClick={signInWithGoogle}>Sign In with Google</button>
    </div>
    );
}